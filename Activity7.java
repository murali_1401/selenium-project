package sampleProject;

import org.testng.annotations.Test;

import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity7 {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void loginTest() {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.id("btnLogin")).click();
        
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        Assert.assertEquals("Welcome Test", loginMessage);
        
        driver.findElement(By.xpath("//*[@id=\"menu_pim_viewMyDetails\"]")).click();
        
  WebDriverWait wait = new WebDriverWait(driver, 60);



	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'My Info')]")));
	driver.findElement(By.xpath("//b[contains(text(),'My Info')]")).click();
	
	JavascriptExecutor js = (JavascriptExecutor)driver;
	js.executeScript("window.scrollBy(0,600)");
	
	driver.findElement(By.xpath("//ul[@id='sidenav']//a[contains(text(),'Qualifications')]")).click();
	
	js.executeScript("window.scrollBy(400,0)");
	driver.findElement(By.cssSelector("#addWorkExperience")).click();
	 
	WebElement company = driver.findElement(By.cssSelector("#experience_employer"));
	company.clear();
	company.sendKeys("IBM");
	//Reporter.log("Company details entered", true);
	
	WebElement jobTitle = driver.findElement(By.cssSelector("#experience_jobtitle"));
	jobTitle.clear();
	jobTitle.sendKeys("BA");
	//Reporter.log("Job title entered", true);
	driver.findElement(By.xpath("//input[@id='btnWorkExpSave']")).click();
	
	
    }
}
        