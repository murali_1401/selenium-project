package sampleProject;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Activity6 
{
	
	WebDriver driver;
	  
	  @BeforeClass
	  public void beforeClass()
	  {
		  driver = new FirefoxDriver();
		  driver.get("http://alchemy.hguy.co/orangehrm");
		  System.out.println("Browser has been launched");
	  }      
	  
	  @Test
	    public void loginTest() {
	        //Find the username and password fields
	        WebElement username = driver.findElement(By.id("txtUsername"));
	        WebElement password = driver.findElement(By.id("txtPassword"));
	        //Enter credentials
	        username.sendKeys("orange");
	        password.sendKeys("orangepassword123");
	        //Click login
	        driver.findElement(By.id("btnLogin")).click();
	        //Read login message
	        String loginMessage = driver.findElement(By.id("welcome")).getText();
	        Assert.assertEquals("Welcome Test", loginMessage);
	        
	        driver.findElement(By.xpath("//*[@id=\"menu_directory_viewDirectory\"]")).click();
	        WebDriverWait wait = new WebDriverWait(driver, 60);
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'Dashboard')]")));
	      driver.findElement(By.xpath("/html/body/div[1]/div[2]/ul/li[9]/a/b")).click();
	    
	      String heading = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[1]/div[1]/h1")).getText();
	      System.out.println("Heading is : " + heading);
	      Assert.assertEquals(heading, "Search Directory");
	  }
   
	  @AfterClass
	  public void afterClass() 
	  {
		  driver.close();
		  System.out.println("Closing the Browser");
	  }
}
