package sampleProject;
import org.testng.annotations.Test;
import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class newEmployee {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void loginTest() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.id("btnLogin")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        Assert.assertEquals("Welcome Test1", loginMessage);
        
        driver.findElement(By.xpath("//*[@id=\"menu_pim_viewPimModule\"]")).click();
        
        WebDriverWait wait = new WebDriverWait(driver, 60); 
        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'PIM')]")));
        
      driver.findElement(By.xpath("/html/body/div[1]/div[2]/ul/li[2]/a/b")).click();
      
      JavascriptExecutor js = (JavascriptExecutor)driver;
      js.executeScript("window.scrollBy(0,600)");
        
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Add Employee')]")));
      
      driver.findElement(By.xpath(" //*[@id=\"menu_pim_addEmployee\"]")).click();
      
        
        WebElement empname = driver.findElement(By.id("firstName"));
       WebElement lname = driver.findElement(By.id("lastName"));
       
       empname.sendKeys("Murali");
       lname.sendKeys("Krishna");
       
     Thread.sleep(1000);
       
       driver.findElement(By.xpath("//*[@id=\"chkLogin\"]")).click();
       
       //driver.findElement(By.id("btnLogin")).click();
       
  
       
       WebElement user = driver.findElement(By.id("user_name"));
       
       user.sendKeys("murali_14");
        
        //Enter credentials

        
       driver.findElement(By.id("btnSave")).click();
       
       
       
       
       driver.findElement(By.xpath("/html/body/div[1]/div[2]/ul/li[1]/a/b")).click();
       
       driver.findElement(By.xpath("//*[@id=\"searchSystemUser_userName\"]")).sendKeys("murali_14");
       
        driver.findElement(By.id("searchBtn")).click();
        
        String confirm = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/form/div[4]/table/tbody/tr/td[2]/a")).getText();
        Assert.assertEquals("murali_14", confirm);
      
     
        
        
        
    }

    @AfterClass
    public void afterClass() {
        //Close browser
        driver.close();
    }
}

