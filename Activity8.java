package sampleProject;

import org.testng.annotations.Test;

import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity8 {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void loginTest() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.id("btnLogin")).click();
        
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        Assert.assertEquals("Welcome Test", loginMessage);
        
        driver.findElement(By.xpath("//*[@id=\"menu_dashboard_index\"]")).click();
       
        driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div[1]/div/div/div/fieldset/div/div/table/tbody/tr/td[4]/div/a/span")).click();
        
        Thread.sleep(1000);
        Select dropdown = new Select(driver.findElement(By.id("applyleave_txtLeaveType")));  
        dropdown.selectByVisibleText("Paid Leave"); 
        
        

  WebElement frmdate = driver.findElement(By.id("applyleave_txtFromDate"));
  
  frmdate.clear();
		  
  
  frmdate.sendKeys("2020-05-15");
  
  WebElement todate = driver.findElement(By.id("applyleave_txtToDate"));
		  
		  todate.clear();
		  
		  todate.sendKeys("2020-05-16");
		  
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scrollBy(200,0)");
			
			//driver.findElement(By.xpath("//ul[@id='sidenav']//a[contains(text(),'Qualifications')]")).click();
		  
		  driver.findElement(By.xpath("//*[@id=\"applyBtn\"]")).submit();
		  
		  driver.findElement(By.id("menu_leave_viewMyLeaveList")).click();
		  
		  WebElement calfrmd = driver.findElement(By.id("calFromDate"));
		  
		  calfrmd.clear();
				  
		  
		  calfrmd.sendKeys("2020-05-25");
		  
		  WebElement caltod = driver.findElement(By.id("calToDate"));
				  
				  caltod.clear();
				  
				  caltod.sendKeys("2020-05-26");
				  
				  
				  driver.findElement(By.xpath("//*[@id=\"btnSearch\"]")).submit();
				  
				  //WebDriverWait wait = new WebDriverWait(driver, 60); 
			        
			       Thread.sleep(1000);
				  
				  String Msg;
				  
				Msg=driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/form/div[3]/table/tbody/tr/td[6]/a")).getText();
		  
				
				  System.out.println("Status of the leave    " +   Msg);
				  
				  
  

	
	
    }
}
        