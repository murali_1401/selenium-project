package sampleProject;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import junit.framework.Assert;

public class Activity14 {
	WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@Test(priority = 0)
	public void login() {
		driver.findElement(By.xpath("//*[@id=\"txtUsername\"]")).sendKeys("orange");
		driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("orangepassword123");
		driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"menu_dashboard_index\"]")).click();
	}

	@Test(priority = 1, enabled = true)
	public void multiplevacancies() throws IOException, CsvException {
		driver.findElement(By.xpath("//a[@id='menu_recruitment_viewRecruitmentModule']")).click();
		driver.findElement(By.xpath("//a[@id='menu_recruitment_viewJobVacancy']")).click();

		CSVReader file = new CSVReader(
				new FileReader("C:\\Users\\MURALIKRISHNAKAMBHAM\\eclipse-workspace\\Selenium\\src\\resources\\vacancy.csv"));
		List<String[]> data = file.readAll();
		Iterator<String[]> itr = data.iterator();
		itr.next();
		while (itr.hasNext()) {
			String[] cell = itr.next();
			for (int i = 0; i < cell.length - 1; i++) {
				driver.findElement(By.xpath("//input[@id='btnAdd']")).click();
				// System.out.println(cell[i]);
				Select title = new Select(driver.findElement(By.xpath("//select[@id = 'addJobVacancy_jobTitle']")));
				title.selectByVisibleText(cell[i]);
				driver.findElement(By.xpath("//input[@id = 'addJobVacancy_name']")).sendKeys(cell[i + 1]);
				driver.findElement(By.xpath("//input[@id = 'addJobVacancy_hiringManager']")).sendKeys(cell[i + 2]);
				driver.findElement(By.xpath("//input[@id='btnSave']")).click();
				driver.findElement(By.xpath("//input[@id='btnBack']")).click();
				i++;
				i++;
			}
		}
		file.close();
	}

	@Test(priority = 2, enabled = true)
	public void verify() throws IOException, CsvException {
		
		//driver.findElement(By.xpath("//a[@id='menu_recruitment_viewRecruitmentModule']")).click();
		//driver.findElement(By.xpath("//a[@id='menu_recruitment_viewJobVacancy']")).click();
		
		CSVReader file = new CSVReader(new FileReader("C:\\Users\\MURALIKRISHNAKAMBHAM\\eclipse-workspace\\Selenium\\src\\resources\\vacancy.csv"));
		List<String[]> data = file.readAll();
		Iterator<String[]> itr = data.iterator();
		itr.next();
		while(itr.hasNext()) {
			String[] cell = itr.next();
			for(int i=0; i<cell.length-1; i++) {
				Select title = new Select(driver.findElement(By.xpath("//select[@id = 'vacancySearch_jobTitle']")));
				title.selectByVisibleText(cell[i]);
				Select vacancy = new Select(driver.findElement(By.xpath("//select[@id = 'vacancySearch_jobVacancy']")));
				vacancy.selectByVisibleText(cell[i+1]);
				Select hiring = new Select(driver.findElement(By.xpath("//select[@id = 'vacancySearch_hiringManager']")));
				hiring.selectByVisibleText(cell[i+2]);
				driver.findElement(By.xpath("//input[@id='btnSrch']")).click();
				String vacancy1 = driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]")).getText();
				String title1 = driver.findElement(By.xpath("//table/tbody/tr[1]/td[3]")).getText();
				String hiring1 = driver.findElement(By.xpath("//table/tbody/tr[1]/td[4]")).getText();
				Assert.assertEquals(cell[i], title1);
				Assert.assertEquals(cell[i+1], vacancy1);
				Assert.assertEquals(cell[i+2], hiring1);
				i++;
				i++;
				
			}
			}
		file.close();
	}
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 20);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
