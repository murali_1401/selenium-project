package sampleProject;
import org.testng.annotations.Test;
import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class Activity10 {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void loginTest() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.id("btnLogin")).click();
        
        driver.findElement(By.xpath("//*[@id=\"menu_recruitment_viewRecruitmentModule\"]")).click();
        
        WebDriverWait wait = new WebDriverWait(driver, 60); 
        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'Recruitment')]")));
        
      driver.findElement(By.xpath("/html/body/div[1]/div[2]/ul/li[5]/a/b")).click();
      
      
      
      JavascriptExecutor js = (JavascriptExecutor)driver;
      js.executeScript("window.scrollBy(0,600)");
        
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Vacancies')]")));
      
      driver.findElement(By.xpath(" //*[@id=\"menu_recruitment_viewJobVacancy\"]")).click();
      
      //Thread.sleep(1000);
      
      driver.findElement(By.xpath(" //*[@id=\"btnAdd\"]")).click();
      
    
    
      
      Select dropdown = new Select(driver.findElement(By.id("addJobVacancy_jobTitle")));  
      dropdown.selectByVisibleText("DevOps Engineer"); 
      
      driver.findElement(By.id("addJobVacancy_name")).sendKeys("Temporary workers");
      
      driver.findElement(By.id("addJobVacancy_hiringManager")).sendKeys("Rahul Khanna");
      
      driver.findElement(By.xpath("//*[@id=\"btnSave\"]")).click();
      
      driver.findElement(By.xpath(" //*[@id=\"btnBack\"]")).click();
      
   
      Select dropdown1 = new Select(driver.findElement(By.id("vacancySearch_jobVacancy")));  
      dropdown1.selectByVisibleText("Temporary workers");
      
      driver.findElement(By.xpath(" //*[@id=\"btnSrch\"]")).click();
      
      String msg = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/form/div[4]/table/tbody/tr/td[2]/a")).getText();
    		  
      Assert.assertEquals("Temporary workers", msg);
      
      
      driver.close();
   
    }
   
 

}
      