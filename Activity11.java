package sampleProject;
import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;




public class Activity11 {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void loginTest() throws InterruptedException, AWTException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.id("btnLogin")).click();
        
        driver.findElement(By.xpath("//*[@id=\"menu_recruitment_viewRecruitmentModule\"]")).click();
        
        //WebDriverWait wait = new WebDriverWait(driver, 60); 
        
       // wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[2][contains(text(),'addCandidate')]")));
        
      driver.findElement(By.xpath("/html/body/div[1]/div[2]/ul/li[5]/a/b")).click();
      
      
      
      JavascriptExecutor js = (JavascriptExecutor)driver;
      js.executeScript("window.scrollBy(0,600)");
        
      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Candidates')]")));
      
      driver.findElement(By.xpath(" //*[@id=\"menu_recruitment_viewCandidates\"]")).click();
      
      Thread.sleep(1000);
      
      driver.findElement(By.xpath(" //*[@id=\"btnAdd\"]")).click();
      
      WebElement fname = driver.findElement(By.id("addCandidate_firstName"));
      WebElement lname = driver.findElement(By.id("addCandidate_lastName"));
      
      //Enter credentials
      fname.sendKeys("Jonathan");
      lname.sendKeys("Wilson");
      
      driver.findElement(By.id("addCandidate_email")).sendKeys("xyz_1234@gmail.com");
      
      //JavascriptExecutor js1 = (JavascriptExecutor)driver;
     // js1.executeScript("scroll(0,150)");
      
     // WebDriverWait wait = new WebDriverWait(driver, 30);
      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ol[@id='addCandidate_resume']/li")));
      
     // Wait for 5 seconds
      //Thread.sleep(5000);
      
     // This will click on Browse button
      
      
      
      
      
      
      WebElement resume = driver.findElement(By.xpath("//input[@id = 'addCandidate_resume']"));
		resume.sendKeys("C:\\Users\\Test1.pdf");
      
    
		driver.findElement(By.id("btnSave")).submit();
		
	      
	      JavascriptExecutor js1 = (JavascriptExecutor)driver;
	      js1.executeScript("scroll(0,150)");
		
		
		//driver.findElement(By.id("btnBack")).click();
	      Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"btnBack\"]")).click();
		
		
		
		driver.findElement(By.xpath("//input[@id = 'candidateSearch_candidateName']")).sendKeys("Jonathan Wilson");
		driver.findElement(By.xpath("//input[@id = 'btnSrch']")).click();
		
		WebElement verify = driver.findElement(By.linkText("Jonathan Wilson"));
		Assert.assertEquals(verify.getText(), "Jonathan Wilson");
      
		
      
      
      
      
      
 
      
    }
}