package sampleProject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



import junit.framework.Assert;

public class Activity9 {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void loginTest() {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.id("btnLogin")).click();
        
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        Assert.assertEquals("Welcome Test", loginMessage);
        
        driver.findElement(By.xpath("//*[@id=\"menu_pim_viewMyDetails\"]")).click();
        
  WebDriverWait wait = new WebDriverWait(driver, 60);



	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'My Info')]")));
	driver.findElement(By.xpath("//b[contains(text(),'My Info')]")).click();
	
	JavascriptExecutor js = (JavascriptExecutor)driver;
	js.executeScript("window.scrollBy(0,600)");
	
	driver.findElement(By.xpath("//ul[@id='sidenav']//a[contains(text(),'Emergency Contacts')]")).click();
	
	
	
	
	// Get all the table row elements from the table 
	List<WebElement> allRows = driver.findElements(By.xpath("//table[@class='table hover']//tr")); 

	// And iterate over them and get all the cells 
	for (WebElement row : allRows) { 
	    List<WebElement> cells = row.findElements(By.tagName("td")); 

	    // Print the contents of each cell
	    for (WebElement cell : cells) {         

	    System.out.println(cell.getText());
	    //Or  below code 
	    //System.out.println(cell.getAttribute("innerHTML"));
	    
	    
	}
	
	   
    }
	
	driver.close();
}
}