package sampleProject;


import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import junit.framework.Assert;

public class Activity13 {
	WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@Test(priority = 0)
	public void login() {
		driver.findElement(By.xpath("//*[@id=\"txtUsername\"]")).sendKeys("orange");
		driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("orangepassword123");
		driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"menu_dashboard_index\"]")).click();
	}

	@Test(priority = 1, enabled = false)
	public void uploadingcsv() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id = 'menu_pim_viewPimModule']")));
		WebElement pim = driver.findElement(By.xpath("//a[@id = 'menu_pim_viewPimModule']"));
		WebElement configuration = driver.findElement(By.xpath("//a[@id= 'menu_pim_Configuration']"));
		WebElement data =driver.findElement(By.xpath("//a[@id='menu_admin_pimCsvImport']")); 
		
		Actions action = new Actions(driver);
		action.moveToElement(pim).moveToElement(configuration).moveToElement(data).click().build().perform();
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='pimCsvImport_csvFile']")));
		
		driver.findElement(By.xpath("//input[@id = 'pimCsvImport_csvFile']")).sendKeys("C:\\Users\\KaranArora\\eclipse-workspace\\Selenium\\src\\resources\\importData.csv");
		driver.findElement(By.xpath("//input[@id = 'btnSave']")).click();
	}	
	
	@Test(priority = 2, enabled = true)
	public void verify() throws IOException, CsvException{
		CSVReader file = new CSVReader(new FileReader("C:\\Users\\MURALIKRISHNAKAMBHAM\\eclipse-workspace\\Selenium\\src\\resources\\importData.csv"));
		List<String[]> data= file.readAll();
		Iterator<String[]> iter= data.iterator();
		iter.next();
		while (iter.hasNext()) {
			String[] cell = iter.next();
			driver.findElement(By.xpath("//a[@id = 'menu_pim_viewPimModule']")).click();
			for (int i=0; i<cell.length-19; i++) {
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath("//input[@id = 'empsearch_employee_name_empName']")));
				WebElement search = driver.findElement(By.xpath("//input[@id = 'empsearch_employee_name_empName']"));
				search.clear();		
				search.sendKeys(cell[i] +" "+cell[i+2]);
				driver.findElement(By.xpath("//input[@id = 'searchBtn']")).click();
				String first = driver.findElement(By.xpath("//table/tbody/tr[1]/td[3]")).getText();
				String last = driver.findElement(By.xpath("//table/tbody/tr[1]/td[4]")).getText();
				Assert.assertEquals(cell[i], first);
				Assert.assertEquals(cell[i+2], last);
				System.out.println("Employee created and verified " + cell[i] + " " + cell[i + 2]); 
				i++;
				i++;
				
			}
		}
		file.close();
	}	

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 20);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
